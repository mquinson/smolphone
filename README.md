# SmolPhone

## Description
This project aims at building a not-so-dumb phone, a device with some of the services that you expect from a smartphone, but that can last about ten years and that can operate one week on a power load.
Our main goal is actually to have fun while discovering how this could go.

## Hardware
The current version (rev A) is built with the following components:
- MCU boad: Nucleo STM32F446RE
- OS: TockOS
- Modem: Quectel EC200T (mPCIe+EVB)
- Screen: Waveshare 3.7inch e-Paper e-Ink Display ([buy](https://www.waveshare.com/3.7inch-e-Paper-HAT.htm), [wiki](https://www.waveshare.com/wiki/3.7inch_e-Paper_HAT_Manual#Working_With_STM32)).
- Keyboard: BB Q20keyboard by Solder Party

The datasheets are in the directory of the same name.

## Connexion

### Screen

| e-Paper | STM32 |
|:-------:|:-----:|
| VCC     | 3.3V  |
| GND     | GND   |
| DIN     | PA7   |
| CLK     | PA5   |
| CS      | PA4   |
| DC      | PA2   |
| RST     | PA1   |
| BUSY    | PA3   |

![]()

## Installation
See TockOS tutorials.

## Roadmap
### Revision A
We are currently experimenting, learning and evaluating the feasibility of some aspects. The hardware is currently a spaghetti dish connecting boards that are sold
preassembled. This is enough to detect the traps that must be solved before printing a custom PCB.

On the software side, we are aiming at a simple proof of concept for now (but we are not there yet). Issuing and receiving a phone call, sending and receiving a text
message, display some basic things on the screen.


### Revision B
The next release will probably include a custom PCB to nicely assemble the elements of revA. But at this point, nothing is fixed yet.

## Contributing
Comments and contributions welcome.

## License
This is an open source project, even if the exact license is still to be determined. Probably some sort of GPL license for the code.

## Project status
Ongoing or even exploratory.
