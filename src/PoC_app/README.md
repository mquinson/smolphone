# Smolphone PoC App

This is the Proof of Concept Smolphone App.
Well for now, it's just blink. Watch it grow!

## Features
Very basic but functionnal call and SMS.

## Open questions/TODO
- Is the sms-manager, that knows which commands to send, the status of the sms... a capsule on top, or part of the modem?
Same for a call manager? Phonebook manager? And other features of the modem.
=> Probably at first we can try a monolithic approach, and split later on when it becomes too big 

- Quectel UART multiplexing:
  - the uart has a command mode and a data mode. It is possible to switch between the two (+++ ATO)
  - in theory we could do it all through the UART.
  - in any case we will not use the data mode in this PoC app.
  - there seem to be a MUX app as well in the Quectel..? Not entirely clear what that does for us.
  - long term it might make sense to use the USB link, but the implementation effort is significatly higher

- How to use usart1 and 3 on top of 2? What is the proper hw driver for the UART?
  - The only one is "Console", and for now platforms don't expect more than one UART/console.
  - Any peripheral should expose higher level interfaces, according to the devs.
  - The good news is that it looks like a lot of the lower level code to use usart1 2 3 is there.
  - that being said, we need to write higher level drivers: a modem capsule and keyboard capsule
  - we need to design the syscall interface for these keyboard and modem capsules
    https://book.tockos.org/development/syscall.html

- currently only 115200 baudrate is supported, without hw control flow
  might be useless for just AT commands, but will the EC200T work without flow control?

- Contrary to what I had in mind, the keyboard is an I2C device, NOT UART. So update everything accordingly in the doc :,)

- The doc needs a much clearer example of how to read/write hardware

## Hardware

### In short
- Nucleo STM32F446RE
- EC200T on EVB (uart 1v8)
- Q20 keyboard kit (i2c 3v3 + irq)
- screen not necessary in the short term: output text to console ie USART2/stlink
- if available, EPD 3,7" waveshare (spi 3v3 + irq)
- level translator board (txs0108e based) for 3v3 to 1v8 for modem UART

### Wires to connect:
  - keyboard:
    - gnd,
	- 3v3,
	- sda,
	- scl,
	- interrupt.
  - Modem:
    - solder on the EVB wires to level translator, attach the level translator to EVB with hotglue.
    - rx
	- tx
	- rts
	- cts
	- RI?
	- gnd
  - Screen:
    - Vcc
	- gnd
	- spi_din
	- spi_clk
	- spi_cs
	- busy
	- Rst?
	- DC (data/command)?


### keyboard
- It is a 3v3 I2C device + Interrupt pin. Connects to I2C1 (seems to be the only one with code in the nucleo tockos code)
- See: https://lectronz.com/products/bb-q20-keyboard-with-trackpad-usb-i2c-pmod
- also https://crates.io/crates/bbq10kbd ?

### Modem
- We use the EC200T as mPCIe module, on the Quectel EVB.
- We solder wires to the testpoints on the board (uart + control flow? + RI?)
- We need a level translator, since the Quectel is 1v8 and stm32 3v3 => dumb TXS0108E based board
- TODO: confirm if the Quectel will work without control flow.

### Screen
TODO


### choosing pins
Our package on nucleo is lqfp64 ; the chip has more options with a larger package obviously

pins that would work for USART1 (datasheet p52, 54) :
- PA8:  USART1_CK
- PA9:  USART1_TX
- PA10: USART1_RX => used for IRQ? Unclear why TODO: check this out
- PA11: USART1_CTS note 1 p56: PA11 and PA12 IO supplied by VDDUSB
- PA12: USART1_RTS note 1 p56: PA11 and PA12 IO supplied by VDDUSB
- PB6: USART1_TX => let's use this one
- PB7: USART1_RX => let's use this one

For all of these USART1 is AF7

Note: we also need a pin for:
- RI: Ring indication, basically interrupt for incoming call
- DCD: data carrier detected, ie modem tells host it lost connexion? look into this
- DTR: data terminal ready, ie host telling modem it want to dial? look into this

pins that would work for I2C1
- PB6: I2C1_SCL
- PB7: I2C1_SDA
- PB8: I2C1_SCL  => let's use this
- PB9: I2C1_SDA  => let's use this
Note: not sure what the FMPI2C1_x pins are - Fast Mode Plus: dont care

For all of these I2C1 is AF4

Note: we also need an interrupt pin for the keyboard, polling would be too ugly


pins that would work for SPI3 (screen)
- PA4: SPI3_NSS (CS)
- PA15: SPI3_NSS
- PB0: SPI3_MOSI
- PB2: SPI3_MOSI
- PB3: SPI3_SCK
- PB4: SPI3_MISO
- PB5: SPI3_MOSI
- PC1: MOSI
PC10: SCK
PC11: MISO
PC12: MOSI
PD0: MOSI
PD6: MOSI

AF is all over the place

## Software

### What do we want to do?
What we need to do in the whole code (ie main.rs + app)
- properly initialize the platform:
  - USART1 for the modem
  - USART2 for the console (already there)
  - USART3 for the keyboard
  
- add basic init for the modem (& keyboard?):
  - wait for boot (RDY)
  - check pin (AT+CPIN?)
  - check network
  - setup RI/incoming call management?
- simple FSM app to test the core features:call and SMS in/out
  - display text menu: "press c to call, press a to answer, press r to read sms, s to send sms, esc to back to this menu" 

### What do we need?
- **a modem capsule**
  - able to handle the modem with AT commands
  - into an uart capsule (? this isnt really a thing it seems, but tbc. would be cleaner to separate low level and basic logic)
  - probably queries/maintains info on some status eg simpin, network, ...
  - probably other IOs too, like RI (ring indication), power management, status led, ...

- **a keyboard capsule**
  - able to handle the Q20 BB keyboard module from Solder Party
  - https://crates.io/crates/bbq10kbd ?
  - otherwise based on the classic API for a keyboard/what they offer otherwise ie the Arduino lib

- **a screen capsule**
  - needs an EPD driver ; probably into SPI capsule plus GPIO for busy pin
  - maybe a higher level capsule eg text scren, or something else
  - for the very first version, can be done with simple text output to the console (ie printf and output into USART2/stlink)

- **aplication code**
  - a very simple FSM based app to showcase core features.
  - probably no need to split in several apps?
  - written in C because why not. could/should be rust.

TODO: more detqils for each of these...

## To build
- follow the tockos tutorials:
  link this app along with your kernel (no tockloader for the nucleo stm32F446RE!)
- remember to use the modified platform file (main.rs) for proper init of the platform




